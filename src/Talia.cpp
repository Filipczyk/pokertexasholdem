#include "Talia.h"

Talia::Talia() {
    for (int kolor = 0; kolor <= 3; kolor++) {
        for (short numer = 1; numer <= 13; numer++) {
            this->karty.push_back(Karta(kolor, numer));
        }
    }
}

void Talia::tasuj() {
    srand(unsigned(time(0)));
    random_shuffle(this->karty.begin(), this->karty.end());
}

void Talia::wypisz() {
    cout << "Karty w talii na stole:" << endl;
    for (int i = 0; i < this->karty.size(); i++) {
        cout << this->karty[i].pobierzKolor() << " " << this->karty[i].pobierzNumer() << "\t";
    }
    cout << endl;
}

void Talia::wypisz(Gracz &obiekt) {
    cout << "Karty w talii gracza '" << obiekt.pobierImie() << "':" << endl;
    for (int i = 0; i < obiekt.karty.size(); i++) {
        cout << obiekt.karty[i].pobierzKolor() << " " << obiekt.
             karty[i].pobierzNumer() << "\t";
    }
    cout << endl;
}

void Talia::wydaj(Gracz &obiekt) {
    obiekt.karty.push_back(this->karty[0]);
    this->karty.erase(this->karty.begin());
}

Talia::~Talia() {
    //dtor
}
