#ifndef GRA_H
#define GRA_H

#include <string>
#include <iostream>
#include <vector>
using namespace std;

#include "Talia.h"
#include "Gracz.h"
#include "Karta.h"

/*
 * G��wna klasa aplikacji, realizacja logiki
 */

class Gra {
public:
    Gra();
    virtual ~Gra();


private:
    int ilosc_graczy;
    vector <Gracz> Gracze;
    Talia Talia_graczy;


    void stworzGraczy();
    void stworzTalie();
    void przetasujTalie();
    void wydajKarty();
    void wyswietlKartyGraczy();
    void wyswietlKartyNaStole();
};

#endif // GRA_H
